# Notes

#### At the very beginning

When I was tasking, I struggled to come up with the first test method.
At that time, there are two main questions to which I faced:

1. What would be the first method being tested?
2. What would be the first test case for this method?

For question 1,
my answers once included the creator of Polynomial, the getter methods of Polynomial and the `add` method.
I finally choose the `add` method,
because I cannot catch up with any direct **reason** or **impetus** of creator and getters
if I don't want a **valuable** [^INVEST] feature of Polynomial.
Pure test against getters or constructors would not make sense because we rarely really need them directly.
These methods could always be driven by features which ware driven by unit tests.

[^INVEST]: Valuable of the INVEST principle.

To unit test the `add` method,
I firstly thought about the test case which adds two *monomials*,
but realized this could involve a *binomial* which must make things no longer as simple as a unit test should cover.

After a not very short consideration,
I turned to test the addition of two constant integers as polynomials
which could at least driven a `Polynomial` class and its `coefficient` field.

#### After I have a concept of Polynomial

Now, I already have a class named Polynomial as my domain.
I still want to keep steps small enough to one single responsibility per unit test.
So I choose to take care of summing two monomials with like terms,
which could trigger implementation of polynomials with characters without involving binomials yet.

I'm here making the assertion as
```
assertThat(foo.add(bar))
    .isNotEqualTo(new Polynomial(-1))
    .isEqualTo(new Polynomial(-1, 'a'));
```
so that it could assure a field `a` in Polynomial.

#### Here I have a notable duplication
Thank God I finally hit my first duplication,
There are duplicated fields
```
private int coefficient;
private char symbol;
private int coefficient1;
private char symbol1;
```
in Polynomial,
so now I need a refactoring which could generate the concept of **Term**.

#### To combine like terms
Here I firstly just want to extract a `likeTermsClassifier` method
to generate an `Object` whose `equals()` method is well defined as a like terms classifier for each `Term`,
so that a `Polynomial` could be sufficiently decoupled from the details of `Term` when classifying like terms.
But I sadly found that currently the classifier `Term#symbol` is also using in constructing Term when combining like terms,
thus an `Object` must be too general.

So I have to introduce another strategy through which it combines like terms
and constructs combined term during grouping, like
```
terms.stream().collect(
    Collectors.groupingBy(
        LikeTermsCombinable::classifier,
        Collectors.reducing(LikeTermsCombinable::combine)
    ))
    ...
```

This also hides detail knowledge about grouping like term into `Term` itself behind a `LikeTermsCombinable` interface,
which allows me to start another test against `Term`,
especially `LikeTermsCombinable`.

Interfaces help unit tests keep light enough.
After an interface extracted,
unit tests against the interface can drive further detail implementations without any changes in consumers.
Now I think it's too early to extract an interface for `LikeTermsCombinable`.
A public method here is enough for segregating details in Term.
I do not really want a interface if I don't need polymorphism.

#### Almost could summing polynomials
Now I almost could summing polynomials,
but I realized there are some mathematical defects,
such as the definition of a term with zero coefficient.

So I now turn to fix these cases.

#### When I'm going to implement multiplying
Since I have already the mathematical structure of polynomials,
I want to go through a bottom-up approach,
which will implement the multiply method of `Term` first,
and then goes to the multiply of `Polynomial`.

#### Untested `PolynomialUtils`
So sad that I made the `PolynomialUtils` without TDD practice
because it was too urgent to have a step showcase to PO, actually my wife.
Now I found it's a litter hard to make `PolynomialUtils` fully configurable.

When I'm considering unit test `PolynomialUtils`,
or a class with different name but working as a generator of `Polynomial` like `PolynomialUtils`,
I realized that it really a bed idea to let this util class hold a random generator itself,
because I could hardly unit test the behaviours of the util.

Then I decided to extract the random generators out of the util,
so that configurations like `MAX_EXPONENT` and `MAX_COEFFICIENT` would be independent from util class,
because they are actually configurations for random generator.
Fortunately, this change would solve the configurable problem of `PolynomialUtils`.

If I were strict in keeping TDD way, I would find this point earlier and have a better design.

#### Random utils should not be static
Now I realized that random utils should never be static.
A util class could be made static only when it really act as an **UTIL**,
which means it should be simple and consistent enough
so that it's behaviors would always been tested integrally
and will never been considered to use a mock.

Random, date, time and configurations in our system are not like that case.
They are apparently not consistent and also, though a little obscure, not simple.
Here I said randoms and date-times not simple,
because there are a lot of dependencies and complex implementations behind,
comparing with StringUtils and CollectionUtils.

However, from a view of TDD,
what directly prevents these utils to be static is their inconsistency.
when we unit test a class, we tend to mock it's dependencies to keep tests light.
But static dependencies are practically hard to mock since they are born global.

Sure, there are some plugins for test frameworks, like JMockito and PowerMock,
which modify bytecode to achieve mock for statics in addition of JUnit's inheritance strategies.
I found 
[this article](https://testing.googleblog.com/2008/12/static-methods-are-death-to-testability.html)
who religiously against static methods.
