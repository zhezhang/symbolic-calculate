package xyz.v2my.symb.algo;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PolynomialUtil {
    public static final PolynomialUtil polynomialGenerator = new PolynomialUtil(new RandomUtil());

    private static final int DEFAULT_MAX_EXPONENT = 5;
    private static final int DEFAULT_MAX_COEFFICIENT = 15;
    private static final int DEFAULT_MAX_FACTOR_NUMBER = 6;
    private static final int DEFAULT_MAX_TERM_NUMBER = 3;

    private final RandomUtil randomUtil;

    public PolynomialUtil(RandomUtil randomUtil) {
        this.randomUtil = randomUtil;
    }

    public Polynomial random() {
        List<Term> terms = IntStream.rangeClosed(1, randomUtil.termNumberUnder(DEFAULT_MAX_TERM_NUMBER))
                .mapToObj(i -> randomTerm())
                .collect(Collectors.toList());
        return Polynomial.of(terms);
    }

    private Term randomTerm() {
        List<Factor> factors = IntStream.rangeClosed(1, randomUtil.factorNumberUnder(DEFAULT_MAX_FACTOR_NUMBER))
                .mapToObj(i -> randomFactor())
                .collect(Collectors.toList());

        return Term.of(randomUtil.coefficientAbsUnder(DEFAULT_MAX_COEFFICIENT), factors);
    }

    private Factor randomFactor() {
        return Factor.of(randomUtil.randomSymbol(), randomUtil.randomExponentUnder(DEFAULT_MAX_EXPONENT));
    }
}
