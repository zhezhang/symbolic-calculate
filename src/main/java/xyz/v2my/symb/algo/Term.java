package xyz.v2my.symb.algo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.function.Predicate.not;

@Getter
@EqualsAndHashCode
@ToString
public class Term {
    public static final Term ZERO = constant(0);

    private int coefficient;
    private List<Factor> factors;

    private Term(int coefficient, List<Factor> factors) {
        this.coefficient = coefficient;
        this.factors = Optional.ofNullable(factors)
                .map(Term::mergeSameBasePower)
                .filter(not(l -> shouldBeConstant(coefficient, l)))
                .orElse(List.of(Factor.ONE));
    }

    private static List<Factor> mergeSameBasePower(List<Factor> factors) {
        return factors.stream()
                .collect(Collectors.groupingBy(Factor::getSymbol, Collectors.reducing(Factor::merge)))
                .values().stream()
                .map(Optional::orElseThrow)
                .collect(Collectors.toList());
    }

    private boolean shouldBeConstant(int coefficient, List<Factor> factors) {
        return 0 == coefficient || factors.isEmpty();
    }

    public static Term of(int coefficient, Symbol symbol) {
        return new Term(coefficient, List.of(Factor.of(symbol, 1)));
    }

    public static Term constant(int coefficient) {
        return new Term(coefficient, List.of(Factor.ONE));
    }

    public static Term of(int coefficient, Factor... factors) {
        return new Term(coefficient, List.of(factors));
    }

    public static Term of(int coefficient, List<Factor> factors) {
        return new Term(coefficient, factors);
    }

    public Object classifier() {
        return new HashSet<>(this.factors);
    }

    public Term combine(Term that) {
        if (!this.classifier().equals(that.classifier())) {
            throw new UnsupportedOperationException();
        }
        return Term.of(this.coefficient + that.coefficient, this.factors);
    }

    public boolean isZero() {
        return 0 == this.coefficient;
    }

    public String toTex() {
        return toTexSignature() +
                toTexCoefficientAbs() +
                this.factors.stream().map(Factor::toTex).collect(Collectors.joining());
    }

    private String toTexSignature() {
        if (this.coefficient > 0) {
            return "+";
        } else if (this.coefficient < 0) {
            return "-";
        } else {
            return "";
        }
    }

    private String toTexCoefficientAbs() {
        if (0 == this.coefficient) {
            return "";
        }
        if (1 != Math.abs(this.coefficient) || this.isConstant()) {
            return String.valueOf(Math.abs(this.coefficient));
        }
        return "";
    }

    private boolean isConstant() {
        return this.factors.stream().noneMatch(factor -> factor != Factor.ONE);
    }

    public Term multiply(Term that) {
        List<Factor> factors = Stream.concat(this.factors.stream(), that.factors.stream()).collect(Collectors.toList());
        return Term.of(this.coefficient * that.coefficient, factors);
    }
}
