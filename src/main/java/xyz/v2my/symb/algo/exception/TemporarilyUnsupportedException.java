package xyz.v2my.symb.algo.exception;

public class TemporarilyUnsupportedException extends RuntimeException {
    public TemporarilyUnsupportedException(String message) {
        super(message);
    }
}
