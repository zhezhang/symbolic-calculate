package xyz.v2my.symb.algo;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class Symbol {
    public static final Symbol _1 = new Symbol('1');

    public static final Symbol a = new Symbol('a');
    public static final Symbol b = new Symbol('b');
    public static final Symbol c = new Symbol('c');
    public static final Symbol d = new Symbol('d');
    public static final Symbol w = new Symbol('w');
    public static final Symbol x = new Symbol('x');
    public static final Symbol y = new Symbol('y');
    public static final Symbol z = new Symbol('z');

    private char symbol;

    private Symbol(char symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return String.valueOf(this.symbol);
    }
}
