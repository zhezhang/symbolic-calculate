package xyz.v2my.symb.algo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.function.Predicate.not;

@EqualsAndHashCode
@ToString
@Getter
public class Polynomial {
    public static final Polynomial ZERO = Polynomial.of(Term.ZERO);

    private List<Term> terms;

    private Polynomial(List<Term> terms) {
        this.terms = Optional.ofNullable(combineLikeTerms(terms))
                .filter(not(List::isEmpty))
                .orElse(List.of(Term.ZERO));
    }

    private List<Term> combineLikeTerms(List<Term> terms) {
        return terms.stream()
                .collect(Collectors.groupingBy(Term::classifier, Collectors.reducing(Term::combine)))
                .values().stream()
                .map(Optional::orElseThrow)
                .filter(not(Term::isZero))
                .collect(Collectors.toList());
    }

    public static Polynomial constant(int coefficient) {
        return new Polynomial(List.of(Term.constant(coefficient)));
    }

    public static Polynomial of(Term... terms) {
        return new Polynomial(List.of(terms));
    }

    public static Polynomial of(List<Term> terms) {
        return new Polynomial(terms);
    }

    public Polynomial add(Polynomial that) {
        List<Term> termList = Stream.of(this.terms, that.terms).flatMap(Collection::stream).collect(Collectors.toList());
        return Polynomial.of(termList);
    }

    public String toTex() {
        String polynomialString = this.terms.stream().map(Term::toTex).collect(Collectors.joining());
        if (polynomialString.startsWith("+")) {
            return polynomialString.substring(1);
        }
        if (polynomialString.isEmpty()) {
            return "0";
        }
        return polynomialString;
    }

    public Polynomial multiply(Polynomial that) {
        List<Term> terms = this.getTerms().stream().map(that::multiply).flatMap(Function.identity()).collect(Collectors.toList());
        return Polynomial.of(terms);
    }

    private Stream<Term> multiply(Term thisTerm) {
        return this.getTerms().stream().map(thisTerm::multiply);
    }
}
