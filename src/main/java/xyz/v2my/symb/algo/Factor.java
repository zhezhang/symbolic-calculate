package xyz.v2my.symb.algo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import xyz.v2my.symb.algo.exception.TemporarilyUnsupportedException;

@EqualsAndHashCode
@ToString
@Getter
public class Factor {
    public static final Factor ONE = Factor.of(Symbol._1, 0);

    private Symbol symbol;
    private int exponent;

    public Factor(Symbol symbol, int exponent) {
        if (exponent < 0) {
            throw new TemporarilyUnsupportedException("exponent should not less than 0");
        }
        this.symbol = symbol;
        this.exponent = exponent;
    }

    public static Factor of(Symbol symbol, int exponent) {
        return new Factor(symbol, exponent);
    }

    static Factor merge(Factor x, Factor y) {
        return of(x.getSymbol(), x.getExponent() + y.getExponent());
    }

    public String toTex() {
        if (this.exponent == 0) {
            return "";
        }
        if (this.exponent == 1) {
            return this.symbol.toString();
        }
        return String.format("%s^{%s}", this.symbol, this.exponent);
    }
}
