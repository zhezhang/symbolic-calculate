package xyz.v2my.symb.algo;

import org.apache.commons.lang3.RandomUtils;

import java.util.List;

import static xyz.v2my.symb.algo.Symbol.a;
import static xyz.v2my.symb.algo.Symbol.b;
import static xyz.v2my.symb.algo.Symbol.c;
import static xyz.v2my.symb.algo.Symbol.d;


class RandomUtil {
    private static final List<Symbol> SYMBOL_SET = List.of(a, b, c, d);

    int termNumberUnder(int maxInclusive) {
        return RandomUtils.nextInt(1, maxInclusive + 1);
    }

    int factorNumberUnder(int maxInclusive) {
        return RandomUtils.nextInt(0, maxInclusive + 1);
    }

    int coefficientAbsUnder(int maxInclusive) {
        return RandomUtils.nextInt(1, maxInclusive + 1) * nextSignature();
    }

    private int nextSignature() {
        return RandomUtils.nextBoolean() ? 1 : -1;
    }

    int randomExponentUnder(int maxInclusive) {
        return RandomUtils.nextInt(0, maxInclusive + 1);
    }

    Symbol randomSymbol() {
        return SYMBOL_SET.get(RandomUtils.nextInt(0, SYMBOL_SET.size()));
    }
}
