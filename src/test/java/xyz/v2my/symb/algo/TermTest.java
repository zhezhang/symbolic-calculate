package xyz.v2my.symb.algo;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static xyz.v2my.symb.algo.Symbol.a;
import static xyz.v2my.symb.algo.Symbol.b;

public class TermTest {

    @Nested
    @DisplayName("when constructing a term")
    class ConstructorTest {
        @Test
        void should_equal_to_zero_when_coefficient_is_zero() {
            Term term = Term.of(0, Factor.of(a, 9), Factor.of(b, 2));

            assertThat(term).isEqualTo(Term.ZERO);
        }

        @Test
        void should_create_term_as_constant_when_factors_is_null_or_empty() {
            Stream.of(
                    Term.of(2, (List<Factor>)null),
                    Term.of(3, List.of())
            ).forEach(term ->
                    assertThat(term.getFactors()).containsOnlyOnce(Factor.ONE)
            );
        }

        @Test
        void should_merge_symbols() {
            Term term = Term.of(3, Factor.of(a, 3), Factor.of(a, 1), Factor.of(b, 3));

            assertThat(term.getFactors()).containsExactlyInAnyOrder(
                    Factor.of(a, 4), Factor.of(b, 3)
            );
        }
    }

    @Nested
    @DisplayName("a term to Tex")
    class ToTexTest {
        @Test
        void should_be_signature_coefficient_and_factors() {
            Term term = Term.of(3, Factor.of(a, 2), Factor.of(b, 4));

            assertThat(term.toTex()).isEqualTo("+3a^{2}b^{4}");
        }

        @Test
        void should_be_coefficient_alone_for_constant() {
            Term constant = Term.constant(-3);

            assertThat(constant.toTex()).isEqualTo("-3");
        }

        @Test
        void should_be_empty_for_zero() {
            Term constant = Term.ZERO;

            assertThat(constant.toTex()).isEqualTo("");
        }

        @Test
        void should_be_only_factors_when_coefficient_is_one() {
            Term term = Term.of(1, Factor.of(a, 2), Factor.of(b, 1));

            assertThat(term.toTex()).isEqualTo("+a^{2}b");
        }

        @Test
        void should_be_signature_factors_when_coefficient_is_minus_one() {
            Term term = Term.of(-1, Factor.of(a, 2), Factor.of(b, 1));

            assertThat(term.toTex()).isEqualTo("-a^{2}b");
        }

        @Test
        void should_be_1_for_constant_one() {
            Term constant = Term.constant(1);

            assertThat(constant.toTex()).isEqualTo("+1");
        }
    }

    @Nested
    @DisplayName("when multiply terms")
    class MultiplyTest {
        @Test
        void should_consist_of_factors_from_both_terms_and_coefficient_equals_multiply_of_both() {
            Term foo = Term.of(4, Factor.of(a, 1), Factor.of(b, 4));
            Term bar = Term.of(3, Factor.of(a, 2), Factor.of(b, 3));

            Term result = foo.multiply(bar);
            assertThat(result.getFactors()).containsExactlyInAnyOrder(Factor.of(a, 3), Factor.of(b, 7));
            assertThat(result.getCoefficient()).isEqualTo(12);
        }
    }
}
