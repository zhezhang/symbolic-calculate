package xyz.v2my.symb.algo;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static xyz.v2my.symb.algo.Symbol.a;
import static xyz.v2my.symb.algo.Symbol.b;
import static xyz.v2my.symb.algo.Symbol.c;

public class PolynomialTest {

    @Nested
    @DisplayName("a single term polynomial")
    class SingleTermPolynomial {
        @Test
        void should_get_a_single_term_polynomial_when_summing_to_single_with_like_terms() {
            Polynomial foo = Polynomial.of(Term.of(3, a));
            Polynomial bar = Polynomial.of(Term.of(-4, a));

            assertThat(foo.add(bar)).isNotEqualTo(Polynomial.constant(-1)).isEqualTo(Polynomial.of(Term.of(-1, a)));
        }

        @Test
        void should_get_a_multiple_term_polynomial_when_summing_to_single_term_with_unlike_terms() {
            Polynomial foo = Polynomial.of(Term.of(3, a));
            Polynomial bar = Polynomial.of(Term.of(3, b));

            assertThat(foo.add(bar)).isEqualTo(Polynomial.of(Term.of(3, a), Term.of(3, b)));
        }

        @Test
        void should_get_a_multiple_term_polynomial_when_summing_to_multiple() {
            Polynomial foo = Polynomial.of(Term.of(3, a));
            Polynomial bar = Polynomial.of(Term.of(4, b), Term.of(5, c));

            assertThat(foo.add(bar)).isEqualTo(Polynomial.of(Term.of(3, a), Term.of(4, b), Term.of(5, c)));
        }

        @Test
        void should_get_a_multiple_term_polynomial_with_like_terms_combined_when_summing() {
            Polynomial foo = Polynomial.of(Term.of(3, a));
            Polynomial bar = Polynomial.of(Term.of(4, b), Term.of(5, a));

            assertThat(foo.add(bar)).isEqualTo(Polynomial.of(Term.of(8, a), Term.of(4, b)));
        }
    }

    @Nested
    @DisplayName("When summing polynomials")
    class SummingPolynomialTest {
        @Test
        void should_get_a_combine_like_terms_when_summing_two_polynomials() {
            Polynomial foo = Polynomial.of(Term.of(3, a), Term.of(3, b), Term.of(-2, c));
            Polynomial bar = Polynomial.of(Term.of(4, a), Term.of(-5, b));

            assertThat(foo.add(bar)).isEqualTo(Polynomial.of(Term.of(7, a), Term.of(-2, b), Term.of(-2, c)));
        }

        @Test
        void should_ignore_zero_terms_in_result_when_summing() {
            Polynomial foo = Polynomial.of(Term.of(4, b), Term.of(3, a), Term.of(3, c));
            Polynomial bar = Polynomial.of(Term.of(-3, a), Term.of(-4, b));

            assertThat(foo.add(bar)).isEqualTo(Polynomial.of(Term.of(3, c)));
        }

        @Test
        void should_have_only_one_zero_term_when_summing_just_opposites() {
            Polynomial foo = Polynomial.of(Term.of(4, b), Term.of(3, a));
            Polynomial bar = Polynomial.of(Term.of(-3, a), Term.of(-4, b));

            assertThat(foo.add(bar)).isEqualTo(Polynomial.ZERO);
        }

        @Test
        void should_get_an_integer_when_summing_two_integers() {
            Polynomial foo = Polynomial.constant(1);
            Polynomial bar = Polynomial.constant(2);

            assertThat(foo.add(bar)).isEqualTo(Polynomial.constant(3));
        }
    }

    @Nested
    @DisplayName("When multiplying polynomials")
    class MultiplyPolynomialTest {
        @Test
        void should_multiply_each_term_of_a_multi_term_polynomial() {
            Polynomial single = Polynomial.of(Term.of(2, Factor.of(a, 2)));
            Polynomial polynomial = Polynomial.of(
                    Term.of(2, Factor.of(a, 2), Factor.of(b, 2)),
                    Term.of(3, Factor.of(a, 3), Factor.of(b, 3)),
                    Term.of(4, Factor.of(b, 3), Factor.of(c, 2)));

            assertThat(single.multiply(polynomial).getTerms()).containsExactlyInAnyOrder(
                    Term.of(4, Factor.of(a, 4), Factor.of(b, 2)),
                    Term.of(6, Factor.of(a, 5), Factor.of(b, 3)),
                    Term.of(8, Factor.of(a, 2), Factor.of(b, 3), Factor.of(c, 2))
            );
        }

        @Test
        void should_multiply_each_term_of_first_polynomial_to_every_term_of_the_second() {
            Polynomial foo = Polynomial.of(
                    Term.of(3, Factor.of(a, 2)),
                    Term.of(5, Factor.of(b, 3)));
            Polynomial bar = Polynomial.of(
                    Term.of(2, Factor.of(a, 2), Factor.of(b, 2)),
                    Term.of(4, Factor.of(b, 3), Factor.of(c, 2)));

            assertThat(foo.multiply(bar).getTerms()).containsExactlyInAnyOrder(
                    Term.of(6, Factor.of(a, 4), Factor.of(b, 2)),
                    Term.of(12, Factor.of(a, 2), Factor.of(b, 3), Factor.of(c, 2)),
                    Term.of(10, Factor.of(a, 2), Factor.of(b, 5)),
                    Term.of(20, Factor.of(b, 6), Factor.of(c, 2)));
        }
    }

    @Nested
    @DisplayName("a polynomial to Tex")
    class ToTexTest {
        @Test
        void should_be_all_its_terms() {
            Polynomial polynomial = Polynomial.of(
                    Term.of(-2, Factor.of(a, 3)),
                    Term.of(3, Factor.of(a, 2)),
                    Term.of(4, Factor.of(a, 1)),
                    Term.constant(5)
            );

            assertThat(polynomial.toTex()).hasSize("-2a^{3}+3a^{2}+4a+5".length()).contains("-2a^{3}", "+3a^{2}", "+4a", "+5");
        }

        @Test
        void should_remove_leading_plus_when_first_term_is_positive() {
            Polynomial polynomial = Polynomial.of(
                    Term.of(3, Factor.of(a, 2)),
                    Term.of(4, Factor.of(a, 1)));

            assertThat(polynomial.toTex()).hasSize("3a^{2}+4a".length());
        }

        @Test
        void should_be_0_for_zero_polynomial() {
            Polynomial zero = Polynomial.ZERO;

            assertThat(zero.toTex()).isEqualTo("0");
        }
    }
}
