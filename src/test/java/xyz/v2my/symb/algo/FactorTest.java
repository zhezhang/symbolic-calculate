package xyz.v2my.symb.algo;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import xyz.v2my.symb.algo.exception.TemporarilyUnsupportedException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static xyz.v2my.symb.algo.Symbol.a;

public class FactorTest {
    @Nested
    @DisplayName("a factor to Tex")
    class ToTexTest {
        @Test
        void should_be_symbol_and_exponent() {
            Factor factor = Factor.of(a, 3);

            assertThat(factor.toTex()).isEqualTo("a^{3}");
        }

        @Test
        void should_be_symbol_alone_when_exponent_is_one() {
            Factor factor = Factor.of(a, 1);

            assertThat(factor.toTex()).isEqualTo("a");
        }

        @Test
        void should_be_empty_when_exponent_is_zero() {
            Factor factor = Factor.of(a, 0);

            assertThat(factor.toTex()).isEmpty();
        }
    }

    @Nested
    @DisplayName("when construct a Factor")
    class ConstructorTest {

        @ParameterizedTest
        @ValueSource(ints = {-1, -2, -10, -123})
        void should_throw_when_exponent_is_less_than_zero(int exponent) {
            Throwable throwable = catchThrowable(() -> Factor.of(a, exponent));

            assertThat(throwable)
                    .isInstanceOf(TemporarilyUnsupportedException.class)
                    .hasMessage("exponent should not less than 0");
        }
    }
}
