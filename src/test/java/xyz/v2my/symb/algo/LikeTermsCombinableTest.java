package xyz.v2my.symb.algo;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static xyz.v2my.symb.algo.Symbol.a;
import static xyz.v2my.symb.algo.Symbol.b;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class LikeTermsCombinableTest {

    @Nested
    @DisplayName("like terms classifier")
    class LikeTermsClassifierTest {

        @Test
        void should_be_symbol_and_its_exponent_for_single_symbol_term() {
            Term term = Term.of(3, Factor.of(a, 2));
            assertThat(term.classifier()).isEqualTo(Set.of(Factor.of(a, 2)));
        }

        @Test
        void should_be_a_list_of_symbols_and_their_exponents_for_multi_symbol_term() {
            Term term = Term.of(3, Factor.of(a, 2), Factor.of(b, 4));
            assertThat(term.classifier()).isEqualTo(Set.of(Factor.of(a, 2), Factor.of(b, 4)));
        }

        @Test
        void should_be_independent_to_order_of_factors_in_term() {
            Term foo = Term.of(3, Factor.of(a, 2), Factor.of(b, 4));
            Term bar = Term.of(3, Factor.of(b, 4), Factor.of(a, 2));
            assertThat(foo.classifier()).isEqualTo(bar.classifier());
        }
    }

    @Nested
    @DisplayName("like terms combination result")
    class LikeTermsCombineTest {
        @Test
        void should_have_coefficient_equals_to_the_sum_of_coefficients_of_two_terms() {
            Term foo = Term.of(4, Factor.of(a, 3), Factor.of(b, 2));
            Term bar = Term.of(-6, Factor.of(b, 2), Factor.of(a, 3));

            assertThat(foo.combine(bar)).isEqualTo(Term.of(-2, Factor.of(a, 3), Factor.of(b, 2)));
        }

        @Test
        void should_throw_exception_when_combining_unlike_terms() {
            Term foo = Term.of(4, Factor.of(a, 3), Factor.of(b, 2));
            Term bar = Term.of(-6, Factor.of(a, 2), Factor.of(b, 2));

            Throwable throwable = catchThrowable(() -> foo.combine(bar));
            assertThat(throwable).isInstanceOf(UnsupportedOperationException.class);
        }
    }
}
