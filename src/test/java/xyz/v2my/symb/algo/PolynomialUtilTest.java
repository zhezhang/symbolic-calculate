package xyz.v2my.symb.algo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static xyz.v2my.symb.algo.Symbol.a;

@ExtendWith(MockitoExtension.class)
class PolynomialUtilTest {

    private PolynomialUtil polynomialUtil;

    @Mock
    private RandomUtil randomUtil;

    @BeforeEach
    void setUp() {
        polynomialUtil = new PolynomialUtil(randomUtil);

        doReturn(1).when(randomUtil).coefficientAbsUnder(anyInt());
        doReturn(1).when(randomUtil).factorNumberUnder(anyInt());
        doReturn(1).when(randomUtil).randomExponentUnder(anyInt());
        doReturn(1).when(randomUtil).termNumberUnder(anyInt());
        doReturn(a).when(randomUtil).randomSymbol();
    }

    @Test
    void should_get_polynomial_based_on_int_suppliers() {
        Polynomial random = polynomialUtil.random();

        assertThat(random).isEqualTo(Polynomial.of(Term.of(1, Factor.of(a, 1))));
    }
}
